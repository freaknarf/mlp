/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork;
import java.util.ArrayList;
/**
 *
 * @author Franck
 */
public class Perceptron {
    ArrayList<Neuron> inputNeurons;

    double output;
    double error;
    double t = .1;
    int totalTrainings = 10000;
    int nTrainings = 0;
    int nTrainingsOK = 0;
    double validRateMin=.98;
    double validRate=0;

    Perceptron(ArrayList inputNeurons) {
        this.inputNeurons = inputNeurons;
    }

    public String trainNeurons(int[] dataSet, int[] expResDataSet) {
        int output = 0;
        int expRes;
        String res = "";

        for (int i = 0; i < inputNeurons.size(); i++) {
            
            
            output = inputNeurons.get(i).activate(inputNeurons.get(i).aggregate(dataSet));
            expRes = expResDataSet[i];
            //Computed error
            this.error = output - expRes;
            //Neuron learn
            this.inputNeurons.get(i).learn(this.error, this.t, dataSet);
            res += output;
            if(this.error==0)nTrainingsOK++;
            nTrainings++;
            
            validRate=(((double)nTrainingsOK)/((double)nTrainings));
            
        }
        return res;
    }
}
