/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Franck
 */
public class NeuralNetwork {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int[][] dataSet = readInput();
        int[][] dataSetResult = resultInput();


        Neuron n0 = new Neuron(15);
        Neuron n1 = new Neuron(15);
        Neuron n2 = new Neuron(15);

        ArrayList<Neuron> inputNeurons = new ArrayList<>();

        inputNeurons.add(n0);
        inputNeurons.add(n1);
        inputNeurons.add(n2);
        
        //on crée le perceptron avec 3 neuronnnes
        Perceptron perceptron = new Perceptron(inputNeurons);
        ////TRAINING////////////////////////////////////////////////////////////
        for (int i = 0; i < perceptron.totalTrainings; i++) {
            for (int j = 0; j < 8; j++) {
                perceptron.trainNeurons(dataSet[j], dataSetResult[j]);
               
            }
             if (perceptron.validRate>perceptron.validRateMin) break;
        }
        
        ////TEST SESSION////////////////////////////////////////////////////////
        System.out.println("THIS IS A TEST : 0 (000)");
        System.out.println(perceptron.trainNeurons(dataSet[0], dataSetResult[0]));
        draw(dataSet[0]);
        System.out.println("THIS IS A TEST : 2 (010)");
        System.out.println(perceptron.trainNeurons(dataSet[2], dataSetResult[2]));
        draw(dataSet[2]);
        System.out.println("THIS IS A TEST : 1 (001)");
        System.out.println(perceptron.trainNeurons(dataSet[1], dataSetResult[1]));
        draw(dataSet[1]);
        System.out.println("THIS IS A TEST : 5 (101)");
        System.out.println(perceptron.trainNeurons(dataSet[5], dataSetResult[5]));
        draw(dataSet[5]);
        System.out.println("THIS IS A TEST : 7 (111)");
        System.out.println(perceptron.trainNeurons(dataSet[7], dataSetResult[7]));
        draw(dataSet[7]);
        System.out.println("THIS IS A TEST : 6 (110)");
        System.out.println(perceptron.trainNeurons(dataSet[6], dataSetResult[6]));
        draw(dataSet[6]);
        System.out.println("THIS IS A TEST : weird 1 (001)");
        System.out.println(perceptron.trainNeurons(dataSet[8], dataSetResult[1]));
        draw(dataSet[8]);
        //System.err.println("THIS IS A TEST : weird 7 (111)");
        //System.err.println(perceptron.trainNeurons(dataSet[9], dataSetResult[7]));
        System.out.println("Valid rate : "+perceptron.validRate);

    }
    //////////////INPUTS/EXPECTED RESULTS///////////////////////////////////////
    /**
     *
     * @return
     */
    public static int[][] readInput() {
        int[] exemple0 = {1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1};//0
        int[] exemple1 = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0};//
        int[] exemple2 = {1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1};
        int[] exemple3 = {1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1};
        int[] exemple4 = {1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1};
        int[] exemple5 = {1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1};
        int[] exemple6 = {1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1};
        int[] exemple7 = {1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1};
        int[] exemple8 = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0};
        int[] exemple9 = {1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1};
        
        int[][] i = {exemple0, exemple1, exemple2, exemple3, exemple4, exemple5, exemple6, exemple7, exemple8,exemple9};
        return i;

    }
static public void draw(int[] input){
for (int i=0;i<input.length;i+=3){
    System.out.println(input[i]+" "+input[i+1]+" "+input[i+2]);
}
    System.out.println();
}
    public static int[][] resultInput() {
        int[] exemple0 = {0, 0, 0};//0
        int[] exemple1 = {0, 0, 1};
        int[] exemple2 = {0, 1, 0};
        int[] exemple3 = {0, 1, 1};
        int[] exemple4 = {1, 0, 0};
        int[] exemple5 = {1, 0, 1};
        int[] exemple6 = {1, 1, 0};
        int[] exemple7 = {1, 1, 1};

        int[][] i = {exemple0, exemple1, exemple2, exemple3, exemple4, exemple5, exemple6, exemple7};
        return i;

    }
}
