package neuralnetwork;
import java.util.ArrayList;
/**
 *
 * @author Franck
 */
public class Neuron {

    public int threshold;
    public ArrayList<Double> weights;

    public Neuron(int size) {
        weights = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            double num = 1 * Math.random();
            num = Math.random() > 0.5 ? -num : num;
            weights.add(num);
        }
    }

    public double aggregate(int[] inputs) {
        double input;
        input = 0;
        for (int i = 0; i < inputs.length; i++) {
            input += (inputs[i]) * (weights.get(i));
        }
        return input;
    }

    public int activate(double input) {
        //fonction d'activation seuil
        threshold = 0;
        if (input > threshold) {
            return 1;
        }
        return 0;
    }

    public void learn(double error, double t, int[] input) {
        for (int i = 0; i < this.weights.size(); i++) {
            double dt = t * error * input[i];//DELTA WEIGHTS
            double oldWeight = this.weights.get(i);
            double newWeight = (oldWeight - dt);
            this.weights.set(i, newWeight);
        }
    }
}
